// const signUp=document.querySelector('#sign-up');
// Login Page Check

function validateForm(event){
    event.preventDefault();

    const email = document.getElementById('email').value;
    const password = document.getElementById('pass').value;

    //error message variables
    
    const errorEmail = document.getElementById('email-error');
    const errorPassword = document.getElementById('password');
    let errorCount=0;
    //check for name and last name
    const nonAlphabeticRegex = /[^a-zA-Z]/;

    
    
    if(!email.includes('@')) {
        errorEmail.textContent = 'Please valid email ID.';
        ++errorCount;
    }
    else{
        errorEmail.textContent = '';
    }
    
    if(password.trim().length <5  || !nonAlphabeticRegex.test(password.trim())) {
        errorPassword.textContent = 'Passwords are of more than 5 Characters (Enter your correct password)';
        ++errorCount;
    }
    else{
        errorPassword.textContent = '';
    }

    
    if(errorCount==0){

        console.log('Form successfully submitted!');
    
        localStorage.setItem('name', firstName);
        window.location.href="https://fake-store-manan.vercel.app/index.html";
    }

}
