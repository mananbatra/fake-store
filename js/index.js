
const menProduct=document.getElementById('men');
const womenProduct=document.getElementById('women');
const jewelaryProduct=document.getElementById('jewelery');
const electronicProduct=document.getElementById('electronics');

const menButton=document.getElementById('menProButton');
const womenButton=document.getElementById('womenProButton');
const jeweleryButton=document.getElementById('jewProButton');
const electronicButton=document.getElementById('eleProButton');

function productRender(value, productContainer){
    fetch('https://fakestoreapi.com/products/')
                .then(res=>res.json())
                .then(json=>{

                    // json.length=0;
                    if(json.length===0)
                    {
                        document.getElementById('noProducts').style.display="flex";
                    
                        const mainCard = document.getElementById('filter');
                        mainCard.style.width = '100%';

                        document.getElementById('load').style.display="none";
                        return;
                    }
                    json.filter((product) =>
                    {
                        return product.category===value;
                    }).forEach((product) =>
                    {
                        // Create the tile element
                        const tileElement = document.createElement('div');
                        tileElement.className = 'tile';

                        // Create the image element
                        const imageElement = document.createElement('img');
                        imageElement.src = product.image;
                        imageElement.alt = '';
                        imageElement.id = 'productImage';

                        // Create the details element
                        const detailsElement = document.createElement('div');
                        detailsElement.className = 'details';

                        // Create the title element
                        const titleElement = document.createElement('div');
                        titleElement.className = 'title';
                        titleElement.textContent = product.title;

                        // Create the price element
                        const priceElement = document.createElement('div');
                        priceElement.className = 'price';
                        priceElement.textContent = `$${product.price}`;

                        // Create the rating element
                        const ratingElement = document.createElement('div');
                        ratingElement.className = 'rating';
                        ratingElement.id = 'rate';
                        ratingElement.innerHTML = `${getStars(product.rating.rate)} - (${product.rating.count} Ratings)`;

                        // Append the image, title, price, and rating elements to the details element
                        detailsElement.appendChild(imageElement);
                        detailsElement.appendChild(titleElement);
                        detailsElement.appendChild(priceElement);
                        detailsElement.appendChild(ratingElement);

                        // Append the details element to the tile element
                        tileElement.appendChild(detailsElement);

                        // Append the tile element to the product container
                        productContainer.appendChild(tileElement);

                        document.getElementById('load').style.display="none";
                        document.getElementById('products').style.display="block";
                    })
                }).catch((error) =>
                {
                    document.getElementById('noProducts').style.display="flex";
                    const categoryElements = document.querySelectorAll('.Category');

                    categoryElements.forEach((element) => {
                        element.style.display = 'none';
                        });
                    
                    const hrElements = document.querySelectorAll('hr');

                    hrElements.forEach((hrElement) => {
                          hrElement.style.display = 'none';
                        });
                    const mainCard = document.getElementById('filter');
                    mainCard.style.width = '100%';

                    document.getElementById('load').style.display="none";
                });
        }

function getStars(rating) {
    rating = Math.round(rating * 2) / 2;
    const output = [];

    let index = rating;
    for (index; index >= 1; index--) {
        output.push(`<i class="fa fa-star" aria-hidden="true" style="color: gold;"></i>&nbsp;`);
    }

    if (index === 0.5) {
        output.push(`<i class="fa fa-star-half-o" aria-hidden="true" style="color: gold;"></i>&nbsp;`);
    }

    for (let index = 5 - rating; index >= 1; index--) {
        output.push(`<i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i>&nbsp;`);
    }

    return output.join('');
}
          


productRender("men's clothing",menProduct);

productRender("women's clothing",womenProduct);

productRender("jewelery",jewelaryProduct);

productRender("electronics",electronicProduct);



menButton.addEventListener("click", function() {

    if(menProduct.style.display === "none")
    {
        menProduct.style.display = "flex";
    }
    else{
        menProduct.style.display = "none"
    }
  
});

womenButton.addEventListener("click", function() {

    if(womenProduct.style.display === "none")
    {
        womenProduct.style.display = "flex";
    }
    else{
        womenProduct.style.display = "none"
    }
});

jeweleryButton.addEventListener("click", function() {

    if(jewelaryProduct.style.display === "none")
    {
        jewelaryProduct.style.display = "flex";
    }
    else{
        jewelaryProduct.style.display = "none"
    }
});


electronicButton.addEventListener("click", function() {

    if(electronicProduct.style.display === "none")
    {
        electronicProduct.style.display = "flex";
    }
    else{
        electronicProduct.style.display = "none"
    }
});

const userName=document.getElementById('sign-up')

const data = localStorage.getItem('name');

if(typeof data === String)
{
    userName.textContent=data;
}