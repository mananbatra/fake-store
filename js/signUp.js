// const signUp=document.querySelector('#sign-up');
// Login Page Check

function validateForm(event){
    event.preventDefault();

    const tos = document.getElementById('check-box');
    const firstName = document.getElementById('first').value;
    const lastName = document.getElementById('last').value;
    const email = document.getElementById('email').value;
    const password = document.getElementById('pass').value;
    const confirmPassword = document.getElementById('repeat-pass').value;

    //error message variables
    const termOfService = document.getElementById('term');
    const errorFirstName = document.getElementById('first-name');
    const errorLastName = document.getElementById('last-name');
    const errorEmail = document.getElementById('email-error');
    const errorPassword = document.getElementById('password');
    const errorRepeatPassword = document.getElementById('repeatPassword');
    let errorCount=0;
    //check for name and last name
    const nonAlphabeticRegex = /[^a-zA-Z]/;

    if(!tos.checked) {
        termOfService.textContent = 'Please agree to the Terms of Service.';
        ++errorCount;
    }
    else{
        termOfService.textContent = '';
    }

    if(firstName.trim().length < 1 ||  nonAlphabeticRegex.test(firstName.trim())) {
        errorFirstName.textContent = 'Please enter valid name (Alphabets Only)';
        ++errorCount;
    }
    else{
        errorFirstName.textContent = '';
    }

    if(lastName.trim().length < 1 || nonAlphabeticRegex.test(lastName.trim())) {
        errorLastName.textContent = 'Please enter valid name (Alphabets Only)';
        ++errorCount;
    }
    else{
        errorLastName.textContent = '';
    }
    
    if(!email.includes('@')) {
        errorEmail.textContent = 'Please valid email ID.';
        ++errorCount;
    }
    else{
        errorEmail.textContent = '';
    }
    
    if(password.trim().length <5  || !nonAlphabeticRegex.test(password.trim())) {
        errorPassword.textContent = 'Password must be of more than 5 Characters (Include AlphaNumeric Values)';
        ++errorCount;
    }
    else{
        errorPassword.textContent = '';
    }

    if(password !== confirmPassword)
    {
        errorRepeatPassword.textContent = 'Passwords does not match!';
        ++errorCount;
    }
    else{
        errorRepeatPassword.textContent = '';
    }
    
    if(errorCount==0){

        console.log('Form successfully submitted!');
    
        localStorage.setItem('name', firstName);
        window.location.href="https://fake-store-manan.vercel.app/index.html";
    }

}
